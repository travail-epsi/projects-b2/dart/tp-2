import 'NE_PAS_TOUCHER/user_input.dart';

void main() {
  print("Programme de calcul de moyenne");
  average();
}

void average() {
  print("Veuillez entrer les notes une part une au clavier.\nQuand vous avez fini merci d'entrer -1");
  var grade = 0;
  var sum = 0, count = 0;
  do {
    grade = readInt("Merci d'entrer la note. (-1 si vous avez fini)");
    if (grade >= 0) {
      sum += grade;
      count++;
    }
  } while (grade != -1);
  var aver = sum / count;
  print("La moyenne est de :\n${aver}");
}
