import 'dart:io';

import 'NE_PAS_TOUCHER/user_input.dart';

void main() {
  print("Table des puissances");
  final size = readInt("Quelle est la taille de votre table ? ");
  print("");
  multiplication(size);
}

void multiplication(int size) {
  for (var line = 1; line <= size; line++) {
    for (var column = 1; column <= size; column++) {
      stdout.write("${line * column} ");
    }
    print("");
  }
}
