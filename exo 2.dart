import 'dart:io';

import 'NE_PAS_TOUCHER/user_input.dart';

void main() {
  final squareSize = readInt("Quelle est la taille du carré ?");
  final char =
      readText("Quel est le caractère utilisé pour dessiner le carré ?");
  print(" ");
  print(" ");
  print(" ");

  drawSquare(char: char, squareSize: squareSize);
}

drawSquare({int squareSize = 5, String char = "* "}) {
  for (int i = 0; i < squareSize; i++) {
    for (int j = 0; j < squareSize; j++) {
      if (i > 0 && i < squareSize - 1) {
        if (j == 0 || j == squareSize - 1) {
          stdout.write(char);
          if (j == squareSize - 1) {
            stdout.write('\n');
          }
        } else {
          stdout.write(' ');
        }
      } else {
        stdout.write(char);
        if (j == squareSize - 1) {
          stdout.write('\n');
        }
      }
    }
  }

}
