import 'dart:math';
import 'NE_PAS_TOUCHER/user_input.dart';

void main() {
  print("Calcul de l'aire d'un cercle");
  final radius = readInt("Quelle est le rayon du cercle ?");
  final unit = readText("Quelle est l'unitée ?");
  getArea(radius, unit);
}

getArea(radius, unit) {
  const pi = 3.14159265;
  var area = pi * pow(radius, 2);
  print("L'aire du cercle est de ${area} ${unit}²");
}
