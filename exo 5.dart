import 'NE_PAS_TOUCHER/user_input.dart';

void main() {
  print("Le jeu de NIM en Dart");
  nim();
}

void nim() {
  var matcheNb = 20,
      player = 1,
      inputMax = 3;
  do {
    print("Il reste ${matcheNb} allumette${(matcheNb > 1 ? "s" : "")}");
    if (inputMax > matcheNb) inputMax = matcheNb;
    final choice = takeMatche(inputMax, player);
    matcheNb -= choice;
    player = changePlayer(player);

    if (player == -1) throw Error();
  } while (matcheNb > 0);
  print("Joueur ${player}, vous avez gagné ! Bien joué ! ");
}

int takeMatche(int inputMax, int player,){
  var choice;
  do {
    //Traitement de la saisie
    if (inputMax != 1) {
      choice = readInt(
          "Joueur ${player}, veuillez retirer entre 1 et ${inputMax} d'allumettes.");
    } else {
      print(
          "Joueur ${player}, vous avez perdu. Il ne reste qu'une allumette.");
      choice = 1;
    }
  } while (choice < 1 || choice > inputMax);
  return choice;
}


int changePlayer(int player) {
  switch (player) {
    case 1:
      return 2;
    case 2:
      return 1;
    default:
      return -1;
  }
}
