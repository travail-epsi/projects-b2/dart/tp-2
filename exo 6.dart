import 'NE_PAS_TOUCHER/user_input.dart';
import 'dart:io';

void main() {
  var number;
  do {
    number = readInt("Quelles est la taille de la pyramide ?");
  } while (number <= 0);
  displayPyramid(number);
}

void displayPyramid(int number) {
  for (int i = 0; i <= number; i++) {
    if (i != number) {
      int variance = number - i;
      escapeDisplay(variance);
    }
    displayGrowingNumbers(i);
    displayDecreasingNumbers(i);
    print(" ");
  }
}

void escapeDisplay(int number) {
  for (var i = 1; i <= number; i++) {
    stdout.write("  ");
  }
}

int getUnit(int number) {
  if (number >= 10) {
    return number % 10;
  } else if (number > 0 && number < 10) {
    return number;
  } else {
    return -1;
  }
}

void displayGrowingNumbers(int number) {
  final limit = 2 * number - 1;
  if (limit > 0) {
    do {
      int unit = getUnit(number);
      if (unit != -1) {
        stdout.write("${unit} ");
        number += 1;
      } else {
        throw new Error();
      }
    } while (limit >= number);
  }
}

void displayDecreasingNumbers(int number) {
  int limit = 2 * (number - 1);
  if (limit > 0) {
    do {
      int unit = getUnit(limit);
      if (unit != -1) {
        stdout.write("${unit} ");
        limit -= 1;
      } else {
        throw new Error();
      }
    } while (limit >= number);
  }
}
